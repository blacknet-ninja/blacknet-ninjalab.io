---
lastmod: 2024-03-13
title: Documentation
additionalHeadTag: |
  <script type="application/ld+json">
    {
      "@context": "https://schema.org",
      "@id": "https://blacknet.ninja/documentation.html#web-page",
      "@type": "WebPage",
      "author": { "@id": "https://blacknet.ninja/team/blacknet1fcemzzxna.html#person" },
      "isPartOf": { "@id": "https://blacknet.ninja/#web-site" }
    }
  </script>
---

## User

- [Run Node](/download.html)
- [Anonymize P2P Traffic](/proxy-and-anonymous-p2p.html)
- [Start Staking](/staking.html)

## Developer

- [Zero-Knowledge Application](/zero-knowledge-application.html)
- [Cross-Consensus Atomic Swap](/atomic-swap.html)
- [Message Signing](/message-signing.html)
- [Multisignature Lock Contract](/multisignature-lock-contract.html)
- [Token Transfer](/token-transfer.html)
- [Transaction Batching](/transaction-batching.html)
- [Trusted Timestamping](/trusted-timestamping.html)
- [Wallet and Node RPC API](/apiv2.html)
