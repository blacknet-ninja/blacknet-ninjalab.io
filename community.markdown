---
lastmod: 2024-06-18
title: Community
additionalHeadTag: |
  <script type="application/ld+json">
  [
    {
      "@context": "https://schema.org",
      "@id": "https://blacknet.ninja/community.html#organization",
      "@type": "Organization",
      "name": "Blacknet",
      "alternateName": "BLN",
      "description": "We are a worldwide community interested in cryptocurrency, and our identities are pseudonymous.",
      "logo": { "@id": "https://blacknet.ninja/#image-object-logo" },
      "url": "https://blacknet.ninja/",
      "sameAs": ["https://gitlab.com/blacknet-ninja", "https://bitcointalk.org/index.php?topic=469640.0"]
    },
    {
      "@context": "https://schema.org/",
      "@id": "https://blacknet.ninja/community.html#image-object-community",
      "@type": "ImageObject",
      "contentUrl": "https://blacknet.ninja/images/community.webp",
      "creator": { "@id": "https://blacknet.ninja/team/blackgo.html#person" },
      "creditText": "Blacknet Team",
      "datePublished": "2024-05-18T14:13:00Z",
      "description": "That's clearly the work of Bablo Bicasso."
    }
  ]
  </script>
---

<style>main ul { list-style: none; }</style>

<img height="200" width="200" src="/images/community.webp" alt="Cyber ninja" style="float: right; margin-left: 10px;">

We are a worldwide community interested in cryptocurrency, and our identities are pseudonymous.
The core members constitute Blacknet Team.

## Exchanges

- [<img src="/images/fontawesome/right-left.svg" alt="" class="fas"/> XeggeX BLN/BTC](https://xeggex.com/market/BLN_BTC)
- [<img src="/images/fontawesome/right-left.svg" alt="" class="fas"/> XeggeX BLN/USDT](https://xeggex.com/market/BLN_USDT)

## Explorers

- [<img src="/images/fontawesome/cubes.svg" alt="" class="fas"/> blnexplorer.io](https://blnexplorer.io/)

## Stake Pools

- [<img src="/images/fontawesome/cubes.svg" alt="" class="fas"/> You can set up our pool from the source code.](https://gitlab.com/blacknet-ninja/blacknet-stakepool)

## Social

- [<img src="/images/fontawesome/comments.svg" alt="" class="fas"/> Matrix chat](https://app.element.io/#/room/#blacknet:matrix.org) room #blacknet:matrix.org
- [<img src="/images/fontawesome/bitcoin.svg" alt="" class="fas"/> BitcoinTalk forum](https://bitcointalk.org/index.php?topic=469640.0) topic 469640
- [<img src="/images/fontawesome/reddit.svg" alt="" class="fas"/> Reddit aggregator](https://old.reddit.com/r/blacknet) subreddit r/blacknet [Onion](https://reddittorjg6rue252oqsxryoxengawnmo46qy4kyii5wtqnwfj4ooad.onion/r/blacknet)
- [<img src="/images/fontawesome/qq.svg" alt="" class="fas"/> Tencent QQ](https://www.qq.com/) group 705602427
- [<img src="/images/fontawesome/gitlab.svg" alt="" class="fas"/> GitLab hosting](https://gitlab.com/blacknet-ninja) group blacknet-ninja
