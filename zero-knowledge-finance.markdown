---
lastmod: 2024-11-16
title: Zero-Knowledge Finance
additionalHeadTag: |
  <script type="application/ld+json">
  [
    {
      "@context": "https://schema.org",
      "@id": "https://blacknet.ninja/zero-knowledge-finance.html#tech-article",
      "@type": "TechArticle",
      "author": { "@id": "https://blacknet.ninja/team/rat4.html#person" },
      "datePublished": "2023-11-10T20:54:56Z",
      "dateModified": "2024-06-15T13:16:00Z",
      "headline": "Zero-Knowledge Finance",
      "image": { "@id": "https://blacknet.ninja/zero-knowledge-finance.html#image-object-zefi" }
    },
    {
      "@context": "https://schema.org/",
      "@id": "https://blacknet.ninja/zero-knowledge-finance.html#image-object-zefi",
      "@type": "ImageObject",
      "contentUrl": "https://blacknet.ninja/images/zefi.webp",
      "creator": { "@id": "https://blacknet.ninja/team/rat4.html#person" },
      "creditText": "Blacknet Team",
      "datePublished": "2024-04-02T16:21:33Z",
      "description": "A creative interpretation of zero-knowledge finance"
    }
  ]
  </script>
---

<img height="200" width="200" src="/images/zefi.webp" alt="Zero-knowledge finance" style="float: right; margin-left: 10px;">

Zero-knowledge finance (<abbr>ZeFi</abbr>) is a form of decentralized finance that does not reveal sensitive financial information such as identifiers, quantities, or rates to offer traditional financial instruments.
It is an opaque and unregulated<sup>[[1]](#reference-1)</sup> financial system being built on modern technologies.
The transparency of decentralized finance has led to numerous problems, exempli gratia, surveillance<sup>[[2]](#reference-2)</sup>, front running<sup>[[3]](#reference-3)</sup>, fungibility<sup>[[4]](#reference-4)</sup>, censorship<sup>[[5]](#reference-5)</sup>.

## Use Cases

- Dark pool is a trading exchange where orders and matches are concealed.<sup>[[6]](#reference-6)</sup>
- Anonymous loan is for lending or borrowing without disparity stemming from regulations.<sup>[[7]](#reference-7)</sup>
- Peer-to-peer leasing is a practice of renting out property when isn't in use or accessing an asset without acquiring ownership. The notable example is [stake pool](/staking.html#stake-pool).

## Development

ZeFi awaits [zApps](/zero-knowledge-application.html) to be on the mainnet.

## References

1. Tim May, <cite id="reference-1">[Untraceable Digital Cash, Information Markets, and BlackNet](https://osaka.law.miami.edu/froomkin/articles/tcmay.htm)</cite>, <time datetime="1997">1997</time>.
1. Xiao Fan Liu, Xin-Jian Jiang, Si-Hao Liu, Chi Kong Tse, <cite id="reference-2">[Knowledge Discovery in Cryptocurrency Transactions: A Survey](https://arxiv.org/abs/2010.01031)</cite>, <time datetime="2020-10-02">October 2, 2020</time>.
1. Massimo Bartoletti, James Hsin-yu Chiang, Alberto Lluch-Lafuente, <cite id="reference-3">[Maximizing Extractable Value from Automated Market Makers](https://arxiv.org/abs/2106.01870)</cite>, <time datetime="2022-07-19">July 19, 2022</time>.
1. Domokos Miklós Kelen, István András Seres, <cite id="reference-4">[Towards Measuring the Traceability of Cryptocurrencies](https://arxiv.org/abs/2211.04259)</cite>, <time datetime="2024-06-01">June 1, 2024</time>.
1. Anton Wahrstätter, Jens Ernstberger, Aviv Yaish, Liyi Zhou, Kaihua Qin, Taro Tsuchiya, Sebastian Steinhorst, Davor Svetinovic, Nicolas Christin, Mikolaj Barczentewicz, Arthur Gervais, <cite id="reference-5">[Blockchain Censorship](https://arxiv.org/abs/2305.18545)</cite>, <time datetime="2023-06-02">June 2, 2023</time>.
1. John Cartlidge, Nigel P. Smart, Younes Talibi Alaoui, <cite id="reference-6">[Multi-Party Computation Mechanism for Anonymous Equity Block Trading: A Secure Implementation of Turquoise Plato Uncross](https://onlinelibrary.wiley.com/doi/10.1002/isaf.1502)</cite>, <time datetime="2020-06-03">June 3, 2020</time>.
1. Poorya Kabir, Tianyue Ruan, <cite id="reference-7">[Reducing Racial Disparities in Consumer Credit: Evidence from Anonymous Loan Applications](https://papers.ssrn.com/sol3/papers.cfm?abstract_id=4364852)</cite>, <time datetime="2023-12-15">December 15, 2023</time>.
