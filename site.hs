--------------------------------------------------------------------------------
--Copyright (c) 2018-2024 Blacknet Team
--Distributed under the MIT License
--------------------------------------------------------------------------------
{-# LANGUAGE OverloadedStrings #-}
import           Data.Monoid (mappend)
import           Hakyll


--------------------------------------------------------------------------------
siteRoot :: String
siteRoot = "https://blacknet.ninja"

config :: Configuration
config = defaultConfiguration {
    destinationDirectory = "public"
}

feedConfig :: FeedConfiguration
feedConfig = FeedConfiguration
    { feedTitle       = "Blacknet"
    , feedDescription = "Blacknet blog"
    , feedAuthorName  = "Blacknet Team"
    , feedAuthorEmail = ""
    , feedRoot        = siteRoot
    }

main :: IO ()
main = hakyllWith config $ do
    match ("robots.txt" .||. "_redirects" .||. "favicon.ico") $ do
        route   idRoute
        compile copyFileCompiler

    match "*.pdf" $ do
        route   idRoute
        compile copyFileCompiler

    match "js/*" $ do
        route   idRoute
        compile copyFileCompiler

    match "images/**" $ do
        route   idRoute
        compile copyFileCompiler

    match "fonts/*" $ do
        route   idRoute
        compile copyFileCompiler

    match "css/*" $ do
        route   idRoute
        compile compressCssCompiler

    match "*.markdown" $ do
        route   $ setExtension "html"
        compile $ pandocCompiler
            >>= loadAndApplyTemplate "templates/default.html" pageCtx

    match "team/*.markdown" $ do
        route   $ setExtension "html"
        compile $ pandocCompiler
            >>= loadAndApplyTemplate "templates/default.html" pageCtx

    match "posts/*" $ do
        route $ setExtension "html"
        compile $ pandocCompiler
            >>= loadAndApplyTemplate "templates/post.html"    postCtx
            >>= saveSnapshot "content"
            >>= loadAndApplyTemplate "templates/default.html" postCtx

    match "archive.html" $ do
        route idRoute
        compile $ do
            posts <- recentFirst =<< loadAll "posts/*"
            let archiveCtx =
                    listField "posts" postCtx (return posts) `mappend`
                    pageCtx

            getResourceBody
                >>= applyAsTemplate archiveCtx
                >>= loadAndApplyTemplate "templates/default.html" archiveCtx

    match ("index.html" .||. "offline-wallet.html" .||. "404.html") $ do
        route   idRoute
        compile $ getResourceBody
            >>= loadAndApplyTemplate "templates/default.html" pageCtx

    match "specs.html" $ do
        route idRoute
        compile $ do
            posts <- recentFirst =<< loadAll "posts/*"
            let indexCtx =
                    listField "posts" postCtx (return posts) `mappend`
                    pageCtx

            getResourceBody
                >>= applyAsTemplate indexCtx
                >>= loadAndApplyTemplate "templates/default.html" indexCtx

    create ["feed.atom"] $ do
        route idRoute
        compile $ do
            let feedCtx = postCtx `mappend` bodyField "description"
            posts <- fmap (take 10) . recentFirst =<< loadAllSnapshots "posts/*" "content"
            renderAtom feedConfig feedCtx posts

    create ["sitemap.xml"] $ do
        route idRoute
        compile $ do
            pages <- loadAll ( "*.markdown"
                          .||. "team/*.markdown"
                          .||. "archive.html"
                          .||. "index.html"
                          .||. "offline-wallet.html"
                          .||. "specs.html"
                             )
            posts <- loadAll "posts/*"
            let sitemapCtx =
                    listField "entries" pageCtx (return (pages ++ posts)) `mappend`
                    defaultContext

            makeItem ""
                >>= loadAndApplyTemplate "templates/sitemap.xml" sitemapCtx

    match "templates/*" $ compile templateBodyCompiler


--------------------------------------------------------------------------------
pageCtx :: Context String
pageCtx =
    constField "siteRoot" siteRoot `mappend`
    defaultContext

postCtx :: Context String
postCtx =
    dateField "date" "%B %e, %Y" `mappend`
    dateField "htmlDate" "%F" `mappend`
    pageCtx
