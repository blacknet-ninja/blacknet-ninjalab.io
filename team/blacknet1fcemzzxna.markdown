---
lastmod: 2024-07-06
title: blacknet1fcemzzxna
additionalHeadTag: |
  <script type="application/ld+json">
  [
    {
      "@context": "https://schema.org",
      "@id": "https://blacknet.ninja/team/blacknet1fcemzzxna.html#person",
      "@type": "Person",
      "image": { "@id": "https://blacknet.ninja/team/blacknet1fcemzzxna.html#image-object-suatmm" },
      "name": "blacknet1fcemzzxna",
      "memberOf": { "@id": "https://blacknet.ninja/community.html#organization" },
      "sameAs": [ "https://gitlab.com/blacknet1fcemzzxna", "https://bitcointalk.org/index.php?action=profile;u=2656506" ]
    },
    {
      "@context": "https://schema.org/",
      "@id": "https://blacknet.ninja/team/blacknet1fcemzzxna.html#image-object-suatmm",
      "@type": "ImageObject",
      "contentUrl": "https://blacknet.ninja/images/team/blacknet1fcemzzxna.webp",
      "creator": { "@id": "https://blacknet.ninja/team/blacknet1fcemzzxna.html#person" },
      "creditText": "blacknet1fcemzzxna",
      "datePublished": "2024-06-18T10:10:10Z",
      "description": "Shut up and take my money"
    }
  ]
  </script>
---

<img height="200" width="200" src="/images/team/blacknet1fcemzzxna.webp" alt="Shut up and take my money" style="float: right; margin-left: 10px;">

Missing in action since about <time datetime="2021">2021</time>.

## Known for

- [Blacknet Desktop](/wallet.html#desktop-wallet)
- [Blacknet Explorer](https://gitlab.com/blacknet-ninja/blacknet-explorer)
- [Blacknet Mobile](/wallet.html#mobile-wallet)
- [Blacknet Stakepool](https://gitlab.com/blacknet-ninja/blacknet-stakepool)

## Social

<ul style="list-style: none;">
<li><a href="https://bitcointalk.org/index.php?action=profile;u=2656506" rel="me">
    <img src="/images/fontawesome/bitcoin.svg" alt="" class="fas"/> BitcoinTalk
</a></li>
<li><a href="https://gitlab.com/blacknet1fcemzzxna" rel="me">
    <img src="/images/fontawesome/gitlab.svg" alt="" class="fas"/> GitLab
</a></li>
</ul>
