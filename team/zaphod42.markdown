---
lastmod: 2024-07-06
title: zaphod42
additionalHeadTag: |
  <script type="application/ld+json">
  [
    {
      "@context": "https://schema.org",
      "@id": "https://blacknet.ninja/team/zaphod42.html#person",
      "@type": "Person",
      "image": { "@id": "https://blacknet.ninja/team/zaphod42.html#image-object-avatar" },
      "name": "zaphod42",
      "memberOf": { "@id": "https://blacknet.ninja/community.html#organization" },
      "sameAs": "https://gitlab.com/zaphod101010"
    },
    {
      "@context": "https://schema.org/",
      "@id": "https://blacknet.ninja/team/zaphod42.html#image-object-avatar",
      "@type": "ImageObject",
      "contentUrl": "https://blacknet.ninja/images/team/zaphod42.webp",
      "creator": { "@id": "https://blacknet.ninja/team/zaphod42.html#person" },
      "creditText": "zaphod42",
      "datePublished": "2024-06-15T017:03:42Z",
      "description": "zaphod42 avatar"
    }
  ]
  </script>
---

<img height="200" width="200" src="/images/team/zaphod42.webp" alt="Avatar" style="float: right; margin-left: 10px;">

I put my avatar on this project logotype.
If there's anything more important than my ego around, I want it caught and shot now.

## Characteristics

- Name: zaphod42
- Weapon: Point of view gun
- Self-modification: Extra head and arm

## Social

<a href="https://gitlab.com/zaphod101010" rel="me">
    <img src="/images/fontawesome/gitlab.svg" alt="" class="fas"/> GitLab
</a>
