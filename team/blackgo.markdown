---
lastmod: 2024-06-13
title: blackgo
additionalHeadTag: |
  <script type="application/ld+json">
  [
    {
      "@context": "https://schema.org",
      "@id": "https://blacknet.ninja/team/blackgo.html#person",
      "@type": "Person",
      "image": { "@id": "https://blacknet.ninja/team/blackgo.html#image-object-anonymous-mask" },
      "name": "blackgo",
      "memberOf": { "@id": "https://blacknet.ninja/community.html#organization" }
    },
    {
      "@context": "https://schema.org/",
      "@id": "https://blacknet.ninja/team/blackgo.html#image-object-anonymous-mask",
      "@type": "ImageObject",
      "contentUrl": "https://blacknet.ninja/images/team/blackgo.webp",
      "creator": { "@id": "https://blacknet.ninja/team/blackgo.html#person" },
      "creditText": "blackgo",
      "datePublished": "2024-06-13T00:00:00Z",
      "description": "Anonymous in mask"
    }
  ]
  </script>
---

<img height="200" width="200" src="/images/team/blackgo.webp" alt="Anonymous in mask" style="float: right; margin-left: 10px;">

Ni hao.
I add oil to Blacknet.
Also I'm the Moderator in chat.
Be nice or be banned.

## Characteristics

- Name: blackgo
- Weapon: Banhammer
- Speed: Delete before reading
