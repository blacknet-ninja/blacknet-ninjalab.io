---
lastmod: 2024-01-01
title: Transaction Batching
additionalHeadTag: |
  <script type="application/ld+json">
    {
      "@context": "https://schema.org",
      "@id": "https://blacknet.ninja/transaction-batching.html#tech-article",
      "@type": "TechArticle",
      "author": { "@id": "https://blacknet.ninja/team/rat4.html#person" },
      "datePublished": "2020-09-24T16:05:11Z",
      "dateModified": "2024-01-01T17:32:17Z",
      "headline": "Transaction Batching"
    }
  </script>
---

In Blacknet, transactions can be combined into a Batch transaction that contains data for multiple transactions.
A batch is not permitted to contain another batch, that is to say non-recursive.
A batch is not permitted to contain only one transaction.
A batched transaction does not include Transaction Header saving 140 bytes and one signature verification.

A combination of Transfer transactions is also known as MassTransfer.
Such RPC API may be implemented in the future.
The equivalent in Bitcoin is `sendmany`.

Remote batching for platform transaction of fungible token may be considered in the future.
In such a case, a user of a [zApp](/zero-knowledge-application.html) is not required to hold an amount of BLN to have a write access to the ledger.
The transaction fee is paid in the zApp token instead.
The protocol permits inclusion of a transaction without fee into a new block by the stakers.
