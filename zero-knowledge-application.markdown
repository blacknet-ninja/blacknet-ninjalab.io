---
lastmod: 2025-03-09
title: Zero-Knowledge Application
additionalHeadTag: |
  <script type="application/ld+json">
  [
    {
      "@context": "https://schema.org",
      "@id": "https://blacknet.ninja/zero-knowledge-application.html#tech-article",
      "@type": "TechArticle",
      "author": { "@id": "https://blacknet.ninja/team/rat4.html#person" },
      "datePublished": "2021-12-21T12:26:51Z",
      "dateModified": "2025-03-09T07:27:25Z",
      "headline": "Zero-Knowledge Application",
      "image": { "@id": "https://blacknet.ninja/zero-knowledge-application.html#image-object-zapp" }
    },
    {
      "@context": "https://schema.org/",
      "@id": "https://blacknet.ninja/zero-knowledge-application.html#image-object-zapp",
      "@type": "ImageObject",
      "contentUrl": "https://blacknet.ninja/images/zapp.webp",
      "creator": { "@id": "https://blacknet.ninja/team/rat4.html#person" },
      "creditText": "Blacknet Team",
      "datePublished": "2024-04-02T16:21:33Z",
      "description": "A creative interpretation of zero-knowledge application"
    }
  ]
  </script>
---

<img height="200" width="200" src="/images/zapp.webp" alt="Zero-knowledge application" style="float: right; margin-left: 10px;">

Zero-knowledge application (<abbr>zApp</abbr>) is a decentralized application that makes use of [privacy-enhancing technologies (PETs)](#privacy-enhancing-technologies), but operates correctly despite incomplete information, due to verifiable computation and arguments of knowledge.

## Use Cases

- [Zero-Knowledge Finance](/zero-knowledge-finance.html)
- Cryptocurrency (<abbr>crypto</abbr>) is a tradable digital asset or digital form of money that only relies on cryptography to solve the double spend problem.<sup>[[1]](#reference-1)</sup>
- Non-fungible token (<abbr>NFT</abbr>) is a unique and non-interchangeable unit or asset possessed digitally.<sup>[[2]](#reference-2)</sup>
- Stablecoin (SC) is a digital currency or commodity pegged to a reference value.<sup>[[3]](#reference-3)</sup>
- Decentralized autonomous organization (DAO) is an entity that makes decisions secretively and independently.<sup>[[4]](#reference-4)</sup>
- Self-sovereign identity (SSI) is a digital identity that gives individuals the absolute control of their digital credentials i.e. without disclosure to authoritative issuers.<sup>[[5]](#reference-5)</sup>
- Game, if turn-based, can naturally run on top of consensus. Real-time game can benefit in less straightforward ways, e.g. proof of speedrun<sup>[[6]](#reference-6)</sup>, proof of hi-score.
- Gamble can ensure fair random<sup>[[7]](#reference-7)</sup> and payments.
- Information market<sup>[[8]](#reference-8)</sup> is a trustless bazaar of mathematically formalizable secrets.

## Privacy-Enhancing Technologies

We highlight some of PETs, whilst note that more specialized schemes and combinations thereof do exist.

**Zero-knowledge proof** (<abbr>ZKP</abbr>) allows to attest the integrity of computation over own secret data.
It is the primary tool in Blacknet, herefrom zApp the abbreviation.

**Homomorphic encryption** (<abbr>HE</abbr>) permits computation over encrypted data.
An interesting consequence is possibility of lightweight wallets without loss of privacy.
This is in fact formulated as oblivious message retrieval, which builds on top of HE.

**Secure multiparty computation** (<abbr>SMC</abbr>) enables joint computation over secrets.
It could be crucial in financial applications, for example, to match bids and asks, keeping the price concealed.

## Development

Development of zApps is ongoing R&D progress.

## Summary

Verifiable computation solves the long standing problem of our design: application composability, also known as, two-way communication between applications.
Zero-knowledge can not only add privacy to widely known dApp types, but also paves the way to new ones.
Most advanced dApps, in fact, don't fit into the pure ZK framework, and need to be accompanied with no less sophisticated FHE or SMC.

## References

1. Satoshi Nakamoto, <cite id="reference-1">[Bitcoin P2P e-cash paper](https://www.metzdowd.com/pipermail/cryptography/2008-October/014810.html)</cite>, <time datetime="2008-10-31">October 31, 2008</time>.
1. Dapper Labs, <cite id="reference-2">[CryptoKitties | Collect and breed digital cats!](https://www.cryptokitties.co/)</cite>, <time datetime="2017">2017</time>.
1. Bitfinex_USD, <cite id="reference-3">[The first dividend paying USD asset](https://nxtforum.org/assets-board/(ann)-(usdbitfnx)-the-first-dividend-paying-usd-asset-(~60-per-anno)/)</cite>, <time datetime="2014-07-22">July 22, 2014</time>.
1. Samer Hassan, Primavera De Filippi, <cite id="reference-4">[Decentralized Autonomous Organization](https://policyreview.info/glossary/DAO)</cite>, <time datetime="2021-04-20">April 20, 2021</time>.
1. Christopher Allen, <cite id="reference-5">[The Path to Self-Sovereign Identity](https://www.lifewithalacrity.com/article/the-path-to-self-soverereign-identity)</cite>, <time datetime="2016-04-26">April 26, 2016</time>.
1. pdg744, <cite id="reference-6">[Re: Run Doom on zkVM](https://github.com/risc0/risc0/issues/621#issuecomment-1694570143)</cite>, <time datetime="2023-08-27">August 27, 2023</time>.
1. evoorhees, <cite id="reference-7">[SatoshiDICE.com - Verified rolls, up to 65,000x winning](https://bitcointalk.org/index.php?topic=77870.0)</cite>, <time datetime="2012-04-24">April 24, 2012</time>.
1. Tim May, <cite id="reference-8">[Untraceable Digital Cash, Information Markets, and BlackNet](https://osaka.law.miami.edu/froomkin/articles/tcmay.htm)</cite>, <time datetime="1997">1997</time>.
