---
lastmod: 2024-07-07
title: Download Full Node Wallet
titleOnlyInHead: 1
additionalHeadTag: |
  <script type="application/ld+json">
  [
    {
      "@context": "https://schema.org",
      "@type": "BreadcrumbList",
      "itemListElement": [
        {
          "@type": "ListItem",
          "position": "1",
          "name": "Wallet",
          "item": { "@id": "https://blacknet.ninja/wallet.html#offer" }
        },
        {
          "@type": "ListItem",
          "position": "2",
          "name": "Full Node",
          "item": { "@id": "https://blacknet.ninja/download.html#software-application" }
        }
      ]
    },
    {
      "@context": "https://schema.org",
      "@type": "BreadcrumbList",
      "itemListElement": [
        {
          "@type": "ListItem",
          "position": "1",
          "name": "Documentation",
          "item": { "@id": "https://blacknet.ninja/documentation.html#web-page" }
        },
        {
          "@type": "ListItem",
          "position": "2",
          "name": "Run Node",
          "item": { "@id": "https://blacknet.ninja/download.html#software-application" }
        }
      ]
    },
    {
      "@context": "https://schema.org",
      "@id": "https://blacknet.ninja/download.html#software-application",
      "@type": "SoftwareApplication",
      "name": "Blacknet",
      "description": "Blacknet is a decentralized zero-knowledge finance application platform with the proof of stake consensus.",
      "applicationCategory": "FinanceApplication",
      "applicationSubCategory": "Cryptocurrency",
      "datePublished": "2018-12-21T19:20:19Z",
      "dateModified": "2024-06-14T18:24:33Z",
      "downloadUrl": "https://gitlab.com/blacknet-ninja/blacknet/-/jobs/7104453347/artifacts/download",
      "fileSize": "27MB",
      "memoryRequirements": "512MiB",
      "operatingSystem": "GNU/Linux, FreeBSD, OpenBSD, Windows, macOS",
      "offers": { "@id": "https://blacknet.ninja/wallet.html#offer" },
      "sameAs": "https://gitlab.com/blacknet-ninja/blacknet",
      "screenshot": [
        { "@id": "https://blacknet.ninja/download.html#image-object-webui-send-screenshot" },
        { "@id": "https://blacknet.ninja/download.html#image-object-webui-transactions-screenshot" },
        { "@id": "https://blacknet.ninja/download.html#image-object-daemon-startup-screenshot" },
        { "@id": "https://blacknet.ninja/download.html#image-object-daemon-staking-screenshot" }
      ],
      "softwareRequirements": "Java",
      "softwareVersion": "0.2.14",
      "storageRequirements": "20GB"
    },
    {
      "@context": "https://schema.org/",
      "@id": "https://blacknet.ninja/download.html#image-object-webui-send-screenshot",
      "@type": "ImageObject",
      "contentUrl": "https://blacknet.ninja/images/webui-send-screenshot.webp",
      "creator": { "@id": "https://blacknet.ninja/team/rat4.html#person" },
      "creditText": "Blacknet Team",
      "datePublished": "2023-09-10T14:43:11Z",
      "description": "Web UI: Send Transaction"
    },
    {
      "@context": "https://schema.org/",
      "@id": "https://blacknet.ninja/download.html#image-object-webui-transactions-screenshot",
      "@type": "ImageObject",
      "contentUrl": "https://blacknet.ninja/images/webui-transactions-screenshot.webp",
      "creator": { "@id": "https://blacknet.ninja/team/rat4.html#person" },
      "creditText": "Blacknet Team",
      "datePublished": "2023-09-10T14:43:11Z",
      "description": "Web UI: List Transactions"
    },
    {
      "@context": "https://schema.org/",
      "@id": "https://blacknet.ninja/download.html#image-object-daemon-startup-screenshot",
      "@type": "ImageObject",
      "contentUrl": "https://blacknet.ninja/images/daemon-startup-screenshot.webp",
      "creator": { "@id": "https://blacknet.ninja/team/rat4.html#person" },
      "creditText": "Blacknet Team",
      "datePublished": "2023-09-10T14:43:11Z",
      "description": "Daemon: Startup"
    },
    {
      "@context": "https://schema.org/",
      "@id": "https://blacknet.ninja/download.html#image-object-daemon-staking-screenshot",
      "@type": "ImageObject",
      "contentUrl": "https://blacknet.ninja/images/daemon-staking-screenshot.webp",
      "creator": { "@id": "https://blacknet.ninja/team/rat4.html#person" },
      "creditText": "Blacknet Team",
      "datePublished": "2023-09-10T14:43:11Z",
      "description": "Daemon: Staking"
    }
  ]
  </script>
---

# Core Wallet

Core Wallet is implemented as a full node that provides a builtin wallet with web UI and RPC API.

<nav class="table-of-content">
- [Latest Version](#latest-version)
- [Minimal Version](#minimal-version)
- [How to Run](#run)
- [System Requirements](#system-requirements)
- [Screenshots](#screenshots)
- [Feed back](#feed-back)
- [Source Code](#source-code)
</nav>

## Latest Version

[<img src="/images/fontawesome/file-zipper.svg" alt="" class="fas"/> Download v0.2.14](https://gitlab.com/blacknet-ninja/blacknet/-/jobs/7104453347/artifacts/download)

- Release type: Minor bugfix update
- Release date: <time datetime="2024-06-14">June 14, 2024</time>
- MD5 checksum: `5537BD9EED11C19AA77018671719DF23`

## Minimal Version

From time to time we have mandatory or strongly recommended updates to activate new features or fix security/stability issues.
In case of doubt, use the latest version.

- Minimal version: v0.2.11
- Release date: <time datetime="2021-05-21">May 21, 2021</time>

## Run

1. Download and unzip the latest release
1. Change directory
    * On UN*X `cd ./blacknet/bin`
    * On Windows `cd .\blacknet\bin`
1. Execute
    * On UN*X `./blacknet`
    * On Windows `.\blacknet.bat`
1. Web interface is available at <a href="http://localhost:8283/" rel="nofollow" target="_blank">http://localhost:8283/</a>

## System Requirements

Prerequisites to run a node.

### Hardware

Resource | Minimum   | Recommended
-------- | --------- | -----------
CPU      | 1 core    | 2 cores
RAM      | 512 MiB   | 1 GiB
Storage  | 20 GB HDD | 20 GB SSD

### Software

<img src="/images/fontawesome/java.svg" alt="" class="fas"/> Java 8 or higher.

- Debian & Ubuntu: `sudo apt-get install default-jre`
- Red Hat & Oracle: `sudo yum install java-11-openjdk`
- SUSE: `sudo zypper install java-11-openjdk`
- Arch: `sudo pacman -S --needed jre-openjdk`
- Gentoo: `sudo emerge -av1 --noreplace virtual/jre`
- FreeBSD: `sudo pkg install openjdk11-jre`
- OpenBSD: `sudo pkg_add jdk`
- macOS & Windows: [OpenJDK](https://jdk.java.net/), [Oracle Java](https://java.com/download/), or other

## Screenshots

- [Web UI: Send Transaction](/images/webui-send-screenshot.webp)
- [Web UI: List Transactions](/images/webui-transactions-screenshot.webp)
- [Daemon: Startup](/images/daemon-startup-screenshot.webp)
- [Daemon: Staking](/images/daemon-staking-screenshot.webp)

## Feed back

[<img src="/images/fontawesome/bug.svg" alt="" class="fas"/> Report a bug](https://gitlab.com/blacknet-ninja/blacknet/-/issues)

[<img src="/images/fontawesome/code-pull-request.svg" alt="" class="fas"/> Send a patch](https://gitlab.com/blacknet-ninja/blacknet/-/merge_requests)

## Source Code

The cryptocurrency and its consensus are written in the Kotlin programming language.

1. Download the source code for building locally.
1. For build instructions see the `README.md` file.

[<img src="/images/fontawesome/file-zipper.svg" alt="" class="fas"/> Download v0.2.14](https://gitlab.com/blacknet-ninja/blacknet/-/archive/v0.2.14/blacknet-v0.2.14.zip)

[<img src="/images/fontawesome/tags.svg" alt="" class="fas"/> Download Previous Release Tags](https://gitlab.com/blacknet-ninja/blacknet/-/tags)
