---
lastmod: 2024-07-07
title: Anonymize P2P Traffic
titleOnlyInHead: 1
additionalHeadTag: |
  <script type="application/ld+json">
    {
      "@context": "https://schema.org",
      "@id": "https://blacknet.ninja/proxy-and-anonymous-p2p.html#tech-article",
      "@type": "TechArticle",
      "author": { "@id": "https://blacknet.ninja/team/rat4.html#person" },
      "datePublished": "2018-12-18T08:44:36Z",
      "dateModified": "2024-07-07T10:08:32Z",
      "headline": "Anonymize P2P Traffic"
    }
  </script>
---

# Proxify or Anonymize P2P Traffic

By default, Blacknet node establishes clearnet connections to other nodes and can automatically use locally available supported overlay networks.
Network configuration is stored in file `blacknet.conf`.

## With I2P

I2P implements garlic routing.<sup>[[1]](#reference-1)</sup>

SAM V3.2 is supported.
ECIES-X25519 encryption is preferred since Blacknet version 0.2.14.
It may be disabled by default in I2P router.
You can enable it in I2P Router Console ➜ Configure I2P Router ➜ Clients ➜ SAM application bridge.

I2P bridge for .b32.i2p connections:

```
i2psamhost=127.0.0.1
i2psamport=7656
```

## With Tor

Tor implements onion routing and relies on directory authorities.<sup>[[2]](#reference-2)</sup>

Onion service v3 is supported since Blacknet version 0.2.11.
Tor control port may be disabled by default in `torrc` config file.

Tor proxy for outgoing .onion connections:

```
torhost=127.0.0.1
torport=9050
```

Tor proxy for outgoing TCP/IP connections:

```
listen=false

proxyhost=127.0.0.1
proxyport=9050
```

Tor control port to listen on .onion address:

```
torcontrol=9051
```

## With SOCKS5

TCP/IP can be proxified using SOCKS5 protocol:

```
listen=false

proxyhost=127.0.0.1
proxyport=9050
```

or disabled:

```
ipv4=false
ipv6=false
listen=false
```

## About Helianthus

Anonymization solutions described above don't employ any Sybil-resistance mechanism.<sup>[[1]](#reference-1)[[2]](#reference-2)</sup>
We plan to tickle this problem in a distant future under a subproject codenamed [Helianthus](https://gitlab.com/blacknet-ninja/blacknet/-/issues/85).

## Regarding Mesh Networks

We plan to add support for mesh networks.
[Let us know](https://gitlab.com/blacknet-ninja/blacknet/-/issues/150) which ones you would like to see.

## References

1. jrandom, <cite id="reference-1">[I2P's Threat Model](https://geti2p.net/en/docs/how/threat-model)</cite>, <time datetime="2004-09-13">September 13, 2004</time>.
1. isabela, <cite id="reference-2">[Safeguarding the Tor network: our commitment to network health and supporting relay operators](https://blog.torproject.org/tor-network-community-health-update/)</cite>, <time datetime="2023-11-20">November 20, 2023</time>.
