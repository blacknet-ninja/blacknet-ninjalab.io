---
lastmod: 2020-11-20
title: Decentralized Trusted Timestamping
titleOnlyInHead: 1
---

# Trusted Timestamping

A timestamp is a proof of existence of a document at a certain time relying on the cryptographic security of the consensus mechanism and the hash function.
Applications may involve a digital signature verification, a defensive publication.
Blockchain is a tamperproof transaction log of a distributed ledger database.
After a rolling checkpoint a timestamp cannot be changed.
Blacknet is a decentralized network that maintains a public blockchain.
Trusted timestamping will be implemented in version 0.3 on the proof of stake blockchain.

## RPC API

RPC API for creating and verifying timestamps will be provided.

- Create timestamp |
--- | ---------

- Verify timestamp |
--- | ---------
Type | GET
Path | /verify/{hash}
Parameters |
hash | hash of document
Returns timestamp of a document with given hash.
