---
lastmod: 2024-04-27
title: Text Message Signing
titleOnlyInHead: 1
additionalHeadTag: |
  <script type="application/ld+json">
    {
      "@context": "https://schema.org",
      "@id": "https://blacknet.ninja/message-signing.html#tech-article",
      "@type": "TechArticle",
      "author": { "@id": "https://blacknet.ninja/team/rat4.html#person" },
      "datePublished": "2020-10-03T18:39:44Z",
      "dateModified": "2024-04-27T19:34:00Z",
      "headline": "Text Message Signing"
    }
  </script>
---

# Message Signing

Blacknet account can sign a message for the purpose of authenticated out-of-band communication.<sup>[[1]](#reference-1)</sup>

The used digital signature scheme is Ed25519-BLAKE2b.<sup>[[2]](#reference-2)</sup><sup>[[3]](#reference-3)</sup>
The message is fed into BLAKE2b-256 hashing function with a prefix `Blacknet Signed Message:\n`.
Verification requires an account address, a message signature, an original message.
Make sure the message is [nonreplayable](/multisignature-lock-contract.html#replay-attack) as needed for your use case.

## RPC

- Sign message |
--- | ---------
Type | POST
Path | /api/v2/signmessage
Parameters |
mnemonic | mnemonic of signing account
message | text message
Returns signature.

- Verify message |
--- | ---------
Type | GET
Path | /api/v2/verifymessage/{from}/{signature}/{message}
Parameters |
from | address of signing account
signature | signature to verify
message | text message
Returns boolean.

## Libraries

- Dart [https://pub.dev/packages/blacknet_lib](https://pub.dev/packages/blacknet_lib)
- Go [https://gitlab.com/blacknet-ninja/blacknetgo-lib](https://gitlab.com/blacknet-ninja/blacknetgo-lib)
- JavaScript [https://www.npmjs.com/package/blacknetjs](https://www.npmjs.com/package/blacknetjs)
- PHP [https://packagist.org/packages/blacknet/lib](https://packagist.org/packages/blacknet/lib)

## RPC-CLI

- Python [https://github.com/hclivess/blacknet_tools](https://github.com/hclivess/blacknet_tools)

## References

1. khal, <cite id="reference-1">[Sign and verify message with bitcoin address and public key](https://bitcointalk.org/index.php?topic=6428.0)</cite>, <time datetime="2011-04-24">April 24, 2011</time>.
1. Daniel Bernstein, Niels Duif, Tanja Lange, Peter Schwabe, Bo-Yin Yang, <cite id="reference-2">[High-speed high-security signatures](https://ed25519.cr.yp.to/ed25519-20110926.pdf)</cite>, <time datetime="2011-09-26">September 26, 2011</time>.
1. Jean-Philippe Aumasson, Samuel Neves, Zooko Wilcox-O’Hearn, Christian Winnerlein, <cite id="reference-3">[BLAKE2: simpler, smaller, fast as MD5](https://www.blake2.net/blake2.pdf)</cite>, <time datetime="2013-01-29">January 29, 2013</time>.
