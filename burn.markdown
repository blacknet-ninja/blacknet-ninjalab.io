---
lastmod: 2024-07-07
title: Announcement of Initial Burn Offering
additionalHeadTag: |
  <script type="application/ld+json">
  [
    {
      "@context": "https://schema.org",
      "@id": "https://blacknet.ninja/burn.html#offer",
      "@type": "Offer",
      "availability": "OnlineOnly",
      "priceSpecification": {
        "@type": "PriceSpecification",
        "minPrice": "10",
        "priceCurrency": "BLK",
        "validFrom": "2018-09-07T00:00:00Z",
        "validThrough": "2018-12-14T00:00:00Z"
      },
      "seller": { "@id": "https://blacknet.ninja/community.html#organization" }
    }
  ]
  </script>
---

Initial distribution will be proportional to burned BlackCoins, with a bonus for early participants.

For first 4 weeks the bonus is 20%, later it decreases by 2% per week.

The minimum required burnt amount per account is 10 BLK.

Burning starts at <time datetime="2018-09-07T00:00:00Z">07 Sep 2018 00:00:00 UTC</time> and ends at <time datetime="2018-12-14T00:00:00Z">14 Dec 2018 00:00:00 UTC</time> (<time datetime="P98D">14 weeks</time>).

## The Distribution
You can verify distribution using [Genesis Tool](https://gitlab.com/blacknet-ninja/genesis-tool): `genesis-tool 2252350 2379599 > genesis.json`

<pre id="distribution">Loading...</pre>

<script type="text/javascript">
function getJSON(url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.responseType = 'json';
    xhr.onload = function() {
        var status = xhr.status;
        if (status === 200) {
          callback(null, xhr.response);
        } else {
          callback(status, xhr.response);
        }
    };
    xhr.send();
}
getJSON('/js/genesis.json',
function(err, data) {

  let distribution = document.getElementById("distribution");
    if (err !== null) {
        distribution.innerHTML = "Network error: " + err;
        return;
    }

    distribution.innerHTML =
      'Last scanned block height ' + data["block"] + "<br><br>";
    for (x in data){
      if (x != "block"){
        distribution.innerHTML += x + ' ' + data[x]/100000000 + "<br>";
      }
    }
});
</script>
