---
lastmod: 2024-04-28
title: Token Transfer
additionalHeadTag: |
  <script type="application/ld+json">
    {
      "@context": "https://schema.org",
      "@id": "https://blacknet.ninja/token-transfer.html#tech-article",
      "@type": "TechArticle",
      "author": { "@id": "https://blacknet.ninja/team/rat4.html#person" },
      "datePublished": "2020-10-09T07:52:16Z",
      "dateModified": "2024-04-28T17:25:00Z",
      "headline": "Token Transfer"
    }
  </script>
---

Blacknet account has a reusable address in Bech32<sup>[[1]](#reference-1)</sup> format with a registered<sup>[[2]](#reference-2)</sup> prefix `blacknet`.
Upgrade to Bech32m is discussed in [GitLab ticket 102](https://gitlab.com/blacknet-ninja/blacknet/-/issues/102).
This is in contrast to the UTXO model where addresses often are one-time.

You may need one account for processing customer payments.
Transfers are designed to be distinguished by a payment id.
A payment id essentially is a short string that uniquely identifies your customer or their payment.
It should be a randomly generated string in order to not need an optional encryption.
If you do not need an id unique by a payment then an option to generate new payment address is ought to be provided for customers.

We recommend waiting at least for 10 confirmations, which represent a consensus of stakers, before processing.
Confirmations are not required among trusted parties.
For API see [Wallet Documentation](/apiv2.html#wallet-api).

## References

1. Pieter Wuille, Greg Maxwell, <cite id="reference-1">[Base32 address format for native v0-16 witness outputs](https://github.com/bitcoin/bips/blob/master/bip-0173.mediawiki)</cite>, <time datetime="2017-03-20">March 20, 2017</time>.
1. Clark Moody, <cite id="reference-2">[Registered human-readable parts for BIP-0173](https://github.com/satoshilabs/slips/blob/10d4a9f9f95882e56cd9e3f21ace149f217f8903/slip-0173.md)</cite>, <time datetime="2020-01-01">January 1, 2020</time>.
