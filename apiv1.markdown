---
lastmod: 2019-12-31
title: Deprecated HTTP API Version 1
additionalHeadTag: |
  <script type="application/ld+json">
    {
      "@context": "https://schema.org",
      "@id": "https://blacknet.ninja/apiv1.html#api-reference",
      "@type": "APIReference",
      "author": { "@id": "https://blacknet.ninja/team/rat4.html#person" },
      "datePublished": "2018-12-17T17:52:01Z",
      "dateModified": "2019-12-31T16:40:19Z",
      "headline": "Deprecated HTTP API Version 1"
    }
  </script>
---

HTTP API version 1 has been deprecated and will be removed in the future.

New code should use [API Version 2](/apiv2.html).

## Changes in Version 2

POST methods use Content-Type: application/x-www-form-urlencoded for parameters.

JSON responses use Content-Type: application/json.

blockdb/get: changed format of returned data.

transaction data: type moved to data array, blockHash renamed to referenceChain.

transfer, burn, lease, cancellease: return HTTP status 400 if tx was rejected.

addpeer: returns boolean.

disconnectpeer: returns boolean, address and port changed to peerId.

Renamed functions |
--- | ---------
peerinfo | peers
nodeinfo | node
blockdb/get | block
blockdb/getblockhash | blockhash
blockdb/getblockindex | blockindex
ledger/get | account
account/generate | generateaccount
address/info | address
mnemonic/info | mnemonic
transaction/raw/send | sendrawtransaction
walletdb/getwallet | wallet/transactions
walletdb/getoutleases | wallet/outleases
walletdb/getsequence | wallet/sequence
walletdb/gettransaction | wallet/transaction
walletdb/getconfirmations | wallet/confirmations
staker/start | startstaking
staker/stop | stopstaking

WebSocket notifications use JSON protocol.
