---
lastmod: 2023-12-26
title: Wallet
titleOnlyInHead: 1
additionalHeadTag: |
  <script type="application/ld+json">
    {
      "@context": "https://schema.org",
      "@id": "https://blacknet.ninja/wallet.html#offer",
      "@type": "Offer",
      "availability": "OnlineOnly",
      "price": "0",
      "priceCurrency": "BLN",
      "seller": { "@id": "https://blacknet.ninja/community.html#organization" }
    }
  </script>
---

<style>main ul { list-style: none; }</style>

# Cryptocurrency Wallet

## Software Wallets

Software cryptocurrency wallet is a program that stores keys and transactions, can send and receive new transactions.
Also, text message signing for out-of-band communication is offered.
(It means that the signed message is not broadcast as a transaction, therefore doesn't require a consensus fee.)

### Core Wallet

The full node for desktop and server runs on a variety of operating systems.

- [<img src="/images/fontawesome/download.svg" alt="" class="fas"/> Go to Download page](/download.html)

### Desktop Wallet

A lightweight wallet.

- [<img src="/images/fontawesome/apple.svg" alt="" class="fas"/> macOS](https://github.com/blacknet-ninja/blacknet-desktop/releases/download/v1.3.1/blacknet-desktop-1.3.1.dmg)
- [<img src="/images/fontawesome/windows.svg" alt="" class="fas"/> Windows](https://github.com/blacknet-ninja/blacknet-desktop/releases/download/v1.3.1/blacknet-desktop-1.3.1.exe)
- [<img src="/images/fontawesome/debian.svg" alt="" class="fas"/> deb](https://github.com/blacknet-ninja/blacknet-desktop/releases/download/v1.3.1/blacknet-desktop-1.3.1.deb)
- [<img src="/images/fontawesome/redhat.svg" alt="" class="fas"/> RPM](https://github.com/blacknet-ninja/blacknet-desktop/releases/download/v1.3.1/blacknet-desktop-1.3.1.rpm)
- [<img src="/images/fontawesome/freebsd.svg" alt="" class="fas"/> FreeBSD](https://github.com/blacknet-ninja/blacknet-desktop/releases/download/v1.3.1/blacknet-desktop-1.3.1.freebsd)

### Mobile Wallet

Mobile Wallet currently is not available on OS repositories, so has to be installed manually.
Actual API endpoints (explorers) may be found on the Community page.

- [<img src="/images/fontawesome/android.svg" alt="" class="fas"/> Android](https://gitlab.com/blacknet-ninja/blacknet-mobile/-/releases)
- [<img src="/images/fontawesome/apple.svg" alt="" class="fas"/> iOS](https://gitlab.com/blacknet-ninja/blacknet-mobile/-/releases)
