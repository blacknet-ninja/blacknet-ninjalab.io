---
lastmod: 2024-02-25
title: Multi-Signature Lock Contract
titleOnlyInHead: 1
additionalHeadTag: |
  <script type="application/ld+json">
    {
      "@context": "https://schema.org",
      "@id": "https://blacknet.ninja/multisignature-lock-contract.html#tech-article",
      "@type": "TechArticle",
      "author": { "@id": "https://blacknet.ninja/team/rat4.html#person" },
      "datePublished": "2020-09-27T14:05:31Z",
      "dateModified": "2024-02-25T17:11:00Z",
      "headline": "Multi-Signature Lock Contract"
    }
  </script>
---

# Multisignature Lock Contract

Multisignature (also called multisig) is an authorization mechanism that requires M signatures of N public keys.<sup>[[1]](#reference-1)</sup>
In Blacknet, it is implemented as a lock that can be created then spent.
Example application of 2-of-3 multisig is a marketplace: buyer, seller, escrow.<sup>[[2]](#reference-2)</sup>

## Replay Attack

A replay attack is an attack, in which a legitimate data is maliciously repeated.
An adversary could try to resubmit a transaction or its data to the network for dishonest execution.
An ordinary transaction signature is committed to the transaction header that cannot be replayed.
A spending multi-signature is committed to the contract id that also cannot be replayed.
For creating multi-signature following data is hashed: sender id, sender sequence number, transaction data index number, transaction data without signatures.

## RPC API

RPC API is not yet implemented.

- Create multisig |
--- | ---------

- Spend multisig |
--- | ---------

## References

1. Satoshi Nakamoto, <cite id="reference-1">[Bitcoin v0.1 released](https://www.metzdowd.com/pipermail/cryptography/2009-January/014994.html)</cite>, <time datetime="2009-01-08">January 8, 2009</time>.
1. jenncloud, <cite id="reference-2">[OpenBazaar Dispute Resolution Guidelines](https://github.com/OpenBazaar/official_site/blob/62faf1b5a0f9982c8551729043dd728bde8a81ac/pages/openbazaar-dispute-resolution-guidelines.md)</cite>, <time datetime="2018-09-19">September 19, 2018</time>.
