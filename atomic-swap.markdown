---
lastmod: 2024-07-22
title: Cross-Consensus Atomic Swap
additionalHeadTag: |
  <script type="application/ld+json">
  [
    {
      "@context": "https://schema.org",
      "@id": "https://blacknet.ninja/atomic-swap.html#tech-article",
      "@type": "TechArticle",
      "author": { "@id": "https://blacknet.ninja/team/rat4.html#person" },
      "datePublished": "2020-10-09T07:52:16Z",
      "dateModified": "2024-07-22T16:44:11Z",
      "headline": "Cross-Consensus Atomic Swap",
      "image": { "@id": "https://blacknet.ninja/atomic-swap.html#image-object-atomic-swap" }
    },
    {
      "@context": "https://schema.org/",
      "@id": "https://blacknet.ninja/atomic-swap.html#image-object-atomic-swap",
      "@type": "ImageObject",
      "contentUrl": "https://blacknet.ninja/images/atomic-swap.webp",
      "creator": { "@id": "https://blacknet.ninja/team/rat4.html#person" },
      "creditText": "Blacknet Team",
      "datePublished": "2024-07-22T16:44:11Z",
      "description": "A creative interpretation of atomic swap"
    }
  ]
  </script>
---

<img height="200" width="200" src="/images/atomic-swap.webp" alt="Cross-consensus atomic swap" style="float: right; margin-left: 10px;">

Hash time lock contract, or <abbr>HTLC</abbr> for short, is a composition of a hash lock and a time lock.<sup>[[1]](#reference-1)</sup>
The hash lock authorizes a claim transaction.
The time lock authorizes a refund transaction.
The result of this contract is either claim or refund for both parties<sup>[[2]](#reference-2)</sup>, assuming that transactions are timely confirmed by the consensus algorithms.
It is also named an atomic swap.

## Hash Lock

Hash lock is a cross-consensus authorization mechanism that requires a preimage of a cryptographic hash.

Implemented types:

- BLAKE2b-256
- SHA-256
- Keccak-256
- RIPEMD-160

## Time Lock

Time lock is a cross-consensus authorization mechanism that requires a specified consensus time or height to pass.

Implemented types:

- Absolute consensus time
- Absolute consensus height
- Relative consensus time
- Relative consensus height

## Summary

Atomic swaps can be seen as two-way cross-consensus communication, albeit limited to exchange of fungible or non-fungible tokens.
As well HTLC is a building block of second layer (L2) solutions such as payment channel networks.

## RPC API

RPC API implementation is targeted for Blacknet version 0.3.

*The proposed API is subject to change due to an [issue 144](https://gitlab.com/blacknet-ninja/blacknet/-/issues/144).*

- Create atomic swap |
--- | ---------
Type | POST
Path | /createswap
Parameters |
mnemonic | mnemonic of wallet
fee | transaction fee
amount | amount to transfer
to | address of receiving account
timelocktype | 0 for absolute time, 1 for absolute height, 2 for relative time, 3 for relative height
timelockdata | time or height depending on type
hashlocktype | 0 for BLAKE2b-256, 1 for SHA-256, 2 for Keccak-256, 3 for RIPEMD-160
hashlockdata | HEX-encoded hash
Returns transaction hash.

- Claim atomic swap |
--- | ---------
Type | POST
Path | /claimswap
Parameters |
mnemonic | mnemonic of wallet
fee | transaction fee
id | address of atomic swap
preimage | HEX-encoded preimage
Returns transaction hash.

- Refund atomic swap |
--- | ----------
Type | POST
Path | /refundswap
Parameters |
mnemonic | mnemonic of wallet
fee | transaction fee
id | address of atomic swap
Returns transaction hash.

## References

1. gmaxwell, <cite id="reference-1">[Re: P2PTradeX: P2P Trading between cryptocurrencies](https://bitcointalk.org/index.php?topic=91843.msg1011980#msg1011980)</cite>, <time datetime="2012-07-06">July 6, 2012</time>.
1. satoshi, <cite id="reference-2">[Re: BitDNS and Generalizing Bitcoin](https://bitcointalk.org/index.php?topic=1790.msg28917#msg28917)</cite>, <time datetime="2010-12-10">December 10, 2010</time>.
